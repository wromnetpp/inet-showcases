<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../samples.css">
	<title>Data Link Visualization Example</title>

</head>

<body>
	<h1><center>Data Link Visualization Example</center></h1>

	<p><h2>Goals</h2></p>
	<p> 
        In computer networks, networked computing devices exchange data with each other using a data link.
        The connections between nodes are established using either cable or wireless media.
        If we look at a network topology, we don't know whether there is a data link between two nodes.
        When a network node receives a packet successfully from another device on the second layer, a data link is established between the nodes.
    </p>
    <p>    
        The example consists of four simulation models, each demonstrating different features of data link visualization.
    </p>
	
    <p><h2>About the Visualizer</h2></p>
    <p> 
        The <var>DatalinkVisualizer</var> keeps track of active data links in a network.
        A link becomes active between two nodes when a packet arrives at the receiver node's data link layer from the source node's data link layer.
        By default links are not displayed, we can enable their visualization by switching the <i>displayLinks</i> parameter to true.
    </p>   
    <p> 
        The links are represented visually by solid arrows.
        The arrow's starting point is the sender node and its endpoint is the receiver node.
        The visualizer has filtering parameters, like <i>nodeFilter</i>, <i>interfaceFilter</i> and <i>packetFilter</i>.
        Using these we can specify which data links are shown. By default all nodes, all interfaces and all packets are considered for displaying data links.
        It is possible to use logical expressions as the filter's parameter, e.g <i>packetFilter = "ping* or tcp*"</i> shows the ping and the tcp data links.
    </p>


	<p><h2>Enabling Visualization of Data Links</h2></p>

    <p>
        In this configuration we show how to enable the visualization with its default settings.
        We visualize data links both in a wired and in a wireless network.
    </p>
    <p>
        The wired network contains two <var>Standardhosts</var>, an <var>IPv4NetworkConfigurator</var> module and a <var>DataLinkVisualizer</var> module.
        In the wireless network the type of the hosts is <var>AdhocHost</var> and there is an additional <var>Ieee80211ScalarRadioMedium</var> module.
    </p>
    <p align="center">
        <img width=600 src="DatalinkVisualizerWiredSimple.png">
    </p>
    <p align="center">
        <img width=600 src="DatalinkVisualizerWirelessSimple.png">
    </p>    
    <p>
        In both configurations the source host pings the destination host. The visualization is enabled by
        setting the <var>displayLinks</var> parameter to true:
    </p>
        
        <div class="snippet">
            *.linkVisualizer.*.displayLinks = true
        </div>
    <p>
        When we start the simulations, here is what happens:
    </p>
    <p align="center">
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=618 height=251 src="DatalinkVisualizerWiredSimple.mp4">
    </p>
    <p align="center">
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=618 height=333 src="DatalinkVisualizerWirelessSimple.mp4">
    </p>
    <p>
        When a packet reaches its destination's data link layer, a data link becomes active between the nodes and an arrow is shown pointing from the sender towards the receiver.
        Then the arrow fades out in real time.
    </p>

   
    <p><h2>Data Link Visualization in a Complex Network</h2></p>
    <p>
        This configuration demonstrates the visualization of data links in a complex network.
        In this simulation we also show how the <i>packetFilter</i> and the <i>nodeFilter</i> parameters work.
        Here is the network for this configuration:
    </p>
    <p align="center">
        <img width=600 src="DatalinkVisualizerComplex.png">
    </p>
    <p>
        The network contains an <var>IPv4NetworkConfigurator</var> module and an <var>IntegratedVisualizer</var> module.
        The topology consists of four switches and four endpoints: two source hosts and two destination hosts.
        The sources ping the destinations.
        The visualization of data links is enabled, but it is filtered. Links are displayed only if a ping message is sent.
        We adjust the <i>fadeOutMode</i> and the <i>fadeOutTime</i> parameters, so the data links don't fade out completely before the next ping messages are sent.
        Here is the configuration for the visualization:
        <div class="snippet">
            *.visualizer.*.dataLinkVisualizer.displayLinks = true<br>
            *.visualizer.*.dataLinkVisualizer.fadeOutMode = "simulationTime"<br>
            *.visualizer.*.dataLinkVisualizer.fadeOutTime = 1.4s<br>
            *.visualizer.*.dataLinkVisualizer.packetFilter = "ping*"<br>
        </div>
    </p>
    <p>
        The following animation shows what happens when we start the simulation:
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=566 height=475 src="DatalinkVisualizerComplexv2.mp4">
    </p>
    <p>
        Only ping messages activate the visualization of data links between the nodes, as set by the <i>packetFilter</i>
        parameter.
        The visualized data links are represented by solid arrows.
        The arrows start to fade out in simulation time, but they don't disappear completely, because the fadeOutTime is greater than the sending interval of ping messages.
        Only one data link can be activated per direction between any two nodes.
        If a data link is already visualized between two nodes and another packet activates a data link between the same nodes, the arrow between those nodes is reinforced.
    </p>
    <p>
        It is possible to display ping data links between a selected set of nodes.
        Nodes can be selected with the <i>nodeFilter</i> parameter.
        In this example we display the data links only between the <var>source1</var> and the <var>destination1</var>.
        Here is how to configure the visualizer:
        <div class="snippet">
            *.visualizer.*.dataLinkVisualizer.displayLinks = true<br>
            *.visualizer.*.dataLinkVisualizer.fadeOutMode = "simulationTime"<br>
            *.visualizer.*.dataLinkVisualizer.fadeOutTime = 1.4s<br>
            *.visualizer.*.dataLinkVisualizer.packetFilter = "ping*"<br>
            *.visualizer.*.dataLinkVisualizer.nodeFilter = "(source1 or etherSwitch* or destination1) and not etherSwitch3"
        </div>
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=670 height=465 src="DatalinkVisualizerNodeFilter.mp4">
    </p>


    <p><h2>Displaying Dynamic Data Links</h2></p>
    <p>
        The goal of this configuration is to display dynamically changing active data links in a wireless environment.
        Here is the network for this configuration:
    </p>
    <p align="center">
        <img src="DynamicDatalinks.png">
    </p>
    <p>
        The type of all nodes is <var>AODVRouter</var>.
        As long as the endpoints of a communication connection have valid routes to each other, AODV does not play any role.
        When a route to a new destination is needed, the node broadcasts a RREQ to find a route to the destination.
        The route is made available by unicasting a RREP back to the origination of the RREQ.
        To see this process, we filter the AODV packets:
        <div class="snippet">
            *.visualizer.*.dataLinkVisualizer.packetFilter = "AODV*"<br>
            *.visualizer.*.dataLinkVisualizer.fadeOutTime = 0.5s
        </div>
        In this video we can see how the nodes exchange routing information:
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=568 height=527 src="DatalinkVisualizerAodv.mp4">
    </p>
    <p>
        These ad hoc nodes are placed randomly on the playground and they move randomly between borders.
        The <var>source</var> node has a ping application, that sends ping messages in every second.
        The communication range is reduced to get a non-trivial path between <var>source</var> and <var>destination</var>.
        Here is the configuration:
        <div class="snippet">
            *.visualizer.*.dataLinkVisualizer.displayLinks = true<br>
            *.visualizer.*.dataLinkVisualizer.packetFilter = "ping*"<br>
            *.visualizer.*.dataLinkVisualizer.fadeOutMode = "simulationTime"<br>
            *.visualizer.*.dataLinkVisualizer.fadeOutTime = 1.4s<br>
        </div>
    </p>
    <p>
        The following animation illustrates what happens when the simulation is run: 
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=582 height=523 src="DatalinkVisualizerDynamic.mp4">
    </p>
    <p>
        After the route is established towards the <var>destination</var>, the <var>source</var> starts to ping.
        When a ping message reaches the next node's data link layer, the link is visualized between the nodes.
        When the route is changed between the <var>source</var> and the <var>destination</var>, new data links are activated.
    </p>


    <p><h2>More Information</h2></p>
    <p>
        This example only demonstrated the key features of data link visualization. For more information, refer to the 
<var>DatalinkVisualizer</var> NED documentation.
    </p>

</body>
</html>
