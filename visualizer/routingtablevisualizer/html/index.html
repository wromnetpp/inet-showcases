<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="../../samples.css">
<title>Routing Table Visualization Example</title>
</head>

<body>

<h1><center>Routing Table Visualization Example</center></h1>

<h2>Goals</h2>

<p>
In a complex network topology, it's difficult to see how a packet would be routed because the relevant data is scattered among network nodes and hidden in their routing tables. Graphically visualizing routing tables in a network presents a lot of information about routing at a glance in a concise way. When routing entries are visualized this way, there's no need to examine individual routing tables, and sift through difficult to understand numeric data. This example consists of 3 simulation models of increasing complexity, each demonstrating different features of routing table visualization.
</p>

<h2>About the visualizer</h2>

<p>
The <code><strong>RoutingTableVisualizer</strong></code> module can visualize routing table entries in the following way. Routing table entries are represented visually by solid arrows.
An arrow going from a source node represents a routing table entry in the source node's routing table. The endpoint node of the arrow is the next hop in
the visualized routing table entry.
Note that in the terminology of this example, the word <i>route</i> means the <em>multihop path</em> that leads from A to B.
</p>

<p>
The visualizer has a <i>destinationFilter</i> parameter, which specifies the destination nodes of the visualization.
The routing entry that best matches a destination in each routing table is visualized by an arrow.
By default, the best matching routing entries towards all destinations from all routing tables are visualized.
The <i>nodeFilter</i> parameter controls which nodes' routing tables should be visualized.
</p>

<p>
The visualizer reacts to changes. For example, when a routing protocol changes a routing entry, or an IP address
gets assigned to an interface by DHCP. The visualizer automatically updates the visualizations according to the specified
filters. Some changes, such as assigning an IP address might change the visualization to a great extent.
</p> 

<h2>Displaying all routing tables</h2>

<p>This configuration demonstrates how to enable the visualization of routing tables, and how the visualization looks like.
<p>
The network for this configuration contains 2 connected <code><strong>StandardHosts</strong></code>, an <code><strong>IPv4Configurator</strong></code> module and a <code><strong>RoutingTableVisualizer</strong></code> module.
The configuration contains one line, which enables the visualization of routing tables with the <i>displayRoutingTables</i> parameter.
All other parameters of the visualizer is left on default.
</p>

<p>When the simulation is run, the network looks like this:</p>

<img src="displayroutes.png">

<p>
In this simple network, there are just two visualized routing entries, in <code><strong>HostA</strong></code> towards <code><strong>HostB</strong></code>, and in <code><strong>HostB</strong></code> towards <code><strong>HostA</strong></code>.
</p>

<p>Here are the routing tables of the two hosts, the visualized entries are highlighted:</p>

<div class="include">
<pre>
Node RoutingTableVisualizerA.hostA
-- Routing table --
Destination      Netmask          Gateway          Iface            Metric
<mark>10.0.0.2         255.255.255.255  *                eth0 (10.0.0.1)  0</mark>
10.0.0.0         255.255.255.252  *                eth0 (10.0.0.1)  20
127.0.0.0        255.0.0.0        *                lo0  (127.0.0.1) 1

Node RoutingTableVisualizerA.hostB
-- Routing table --
Destination      Netmask          Gateway          Iface            Metric
<mark>10.0.0.1         255.255.255.255  *                eth0 (10.0.0.2)  0</mark>
10.0.0.0         255.255.255.252  *                eth0 (10.0.0.2)  20
127.0.0.0        255.0.0.0        *                lo0  (127.0.0.1) 1
</pre>
</div>

<p>
TODO: ha netmaskRoutes = "" akkor csak egy bejegyzés van
</p>

<p>Note that you can click on an arrow, and the corresponding routing table entry is shown in the inspector window.</p>

<h2>Filtering routing table entries</h2>

<p>
The goal of this configuration is to demonstrates the use of the <i>nodeFilter</i> and <i>destinationFilter</i> parameters.
</p>

<p>
The network for this configuration looks like the following:
</p>

<img src="filtersnetwork.png">

<p>
It consists of a router connected to a switch. Two <code><strong>StandardHosts</strong></code> are connected to the switch, and two additional <code><strong>StandardHosts</strong></code> are connected to the router. The visualizer module is an <code><strong>IntegratedVisualizer</strong></code>, which contains all available visualizers as submodules.
</p>

</p>
The goal is to only visualize the routes going from <code><strong>host2</strong></code> to <code><strong>host3</strong></code>.
To this end, the <i>destinationFilter</i> parameter is set to <code><strong>host3</strong></code>.
To narrow down the visualized routes to the ones that lead from <code><strong>host2</strong></code>, the <i>nodeFilter</i> parameter
is specified as <code><strong>"not(host1 or host4)"</strong></code>. This instructs the visualizer not to visualize the routing tables of <code><strong>host1</strong></code> and <code><strong>host4</strong></code>.
The routing table of <code><strong>router</strong></code> needs to be visualized, because the route from <code><strong>host2</strong></code> to <code><strong>host3</strong></code> leads through it.
</p>

<p>This is what the visualized routing entries look like:</p>

<img src="routes.png">

<p>The defaults of the <code><strong>nodeFilter</strong></code> and <code><strong>destinationFilter</strong></code> parameters is <code><strong>"*"</strong></code>, which means the best matching routing entries towards all destinations from all routing tables are visualized. With the default settings, the network would look like the following:</p>

<img src="fullmesh.png">

<p>
You might have noticed that the arrows don't go through the switch. This is because L2 devices, like switches and access points, don't have network layers, thus don't have IP addresses or routing tables. They are
effectively transparent for the route visualization arrows. The visualizer could, in theory, know that the packets will take a path that goes through
the switch. This is true in this situation, but in others there may be multiple interconnected switches, and more paths that the packets can take.
However, the pathfinding in that case is not a network layer level problem.
</p>

<p>
The visualizer's parameters can be changed in the runtime environment, and the changes take effect immediatelly. Just select
the <var>RoutingTableVisualizer</var> module, and the parameters are listed in the inspector panel:
</p>

<img class="screen" src="parameters.png">

<h2>Visualizing changing routing tables</h2>

<p>
The visualizer automatically reacts to changes in the routing table of hosts selected with the <i>nodeFilter</i> parameter.
In this configuration, the routing tables are changed by AODV (Advanced On-Demand Vector Routing Protocol).
</p>

<p>
Here is the network for this configuration:
</p>

<img src="dynamicnetwork.png">

<p>
The network contains a series of <code><strong>AODVRouters</strong></code>. These are mobile hosts that have AODV and
IP forwarding enabled. Six of the hosts are laid out in a chain, and are stationary.
Their communication ranges are specified so that each host can only reach the adjacent hosts.
The <code><strong>destinationHost</strong></code> moves up and down along the chain, and is only in the communication range
of 1 or 2 nearby hosts.
</p>

<p>
We want the AODV protocol to configure the routing tables, so the network configurator is instructed not to add static routes.
The <code><strong>sourceHost</strong></code> is configured to ping <code><strong>destinationHost</strong></code>. Since each host is capable of reaching the adjacent hosts only, the ping
packets are relayed to <code><strong>destinationHost</strong></code> through the chain. As the network topology changes because of node mobility, the AODV protocol dynamically configures the routing tables.
</p>

<p>The following animation depicts what happens when the simulation is run.</p>

<img src="routingtablevisualizer2.gif">

<p>When <code><strong>destinationHost</strong></code> starts to move downwards, packets get routed along the chain to the host that is currently adjacent to <code><strong>destinationHost</strong></code>. Finally, this host relays the packets to <code><strong>destinationHost</strong></code>. When <code><strong>destinationHost</strong></code> reaches the bottom of the playground
and turns back, the routing entries in the lower hosts don't change. This is because AODV is an on-demand protocol, thus routing tables are only changed when it is required. On the way back, the lower hosts are not taking part of the packet relay, and the unused entries remain in their routing tables. When <code><strong>destinationHost</strong></code> gets to the top of the
playground, the process starts over again. Routing tables are dynamically reconfigured by AODV to relay the packets along the chain to <code><strong>destinationHost</strong></code>. The visualizer automatically reacts to changes in the routing tables, and updates the visualization accordingly.

<h2>More information</h2>

<p>
This example only demonstrated the key features of routing table visualization. For more information, refer to the 
<code><strong>RoutingTableVisualizer</strong></code> NED documentation.
</p>

</body>
</html>