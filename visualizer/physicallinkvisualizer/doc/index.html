<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../samples.css">
	<title>Physical Link Visualizer Example</title>
</head>

<body>
	<h1><center>Physical Link Visualization Example</center></h1>

	<p><h2>Goals</h2></p>
    <p> 
        In computer networks, networked computing devices exchange data with each other over a physical link.
        The physical connection between nodes are established using either cable or wireless media.
        In wireless networks we can't see whether it is possible to communicate through the radio medium.
        Without displaying physical links, we need to examine the console to find out whether the physical link is established between the nodes.
    </p>
    <p>    
        The example consists of three simulation models, each demonstrating different features of physical link visualization.
    </p>


    <p><h2>About the Visualizer</h2></p>
    <p>
        When a packet is received by a node flawlessly in the physical layer, a physical link is established between the transmitting and the receiving nodes.
        The <var>PhysicalLinkVisualizer</var> keeps track of active physical links in a network.
        A link becomes active between two nodes when a packet arrives at the receiver node's physical link layer from the source node's physical link layer.
        By default links are not displayed, we can enable their visualization by switching the <i>displayLinks</i> parameter to true.
    </p>
    <p> 
        The links are represented visually by dotted arrows.
        The arrow's starting point is the sender node and its endpoint is the receiver node.
        The visualizer has filtering parameters, like <i>nodeFilter</i>, <i>interfaceFilter</i> and <i>packetFilter</i>.
        Using these we can specify which physical links are shown. By default all nodes, all interfaces and all packets are considered for displaying physical links.
        It is possible to use logical expressions as the filter's parameter, e.g <i>packetFilter = "ping* or tcp*"</i> shows the ping and the tcp physical links.
    </p>     


	<p><h2>Enabling Visualization of Physical Links</h2></p>
    <p>
        This configuration demonstrates how to enable the visualization of physical links.
    </p>
    <p>
        We use this simple network for this configuration:
    </p>
    <p>
        <img class="screen" src="PhysicalLinkVisualizerSimple.png">
    </p>
    <p>
        The network contains two <var>Adhochosts</var>. The network also has a <var>PhysicalLinkVisualizer</var>, an <var>IPv4NetworkConfigurator</var> and an <var>Ieee80211ScalarRadioMedium</var> module.<br>
        The <var>source</var> pings the <var>destination</var>.
        This line of code enables the visualization:
        <div class="snippet">
            *.linkVisualizer.*.displayLinks = true
        </div>
    </p>
    <p>
        The following animation depicts what happens when we start the simulation:
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=544 height=318 src="EnablingPhysicalLinks.mp4">
    </p>
    <p>
        When one node receives any packet flawlessly from the other node, a physical link is activated between the nodes.
        It's represented by an arrow, that starts from the transmitter node and points to the receiver node.
    </p>


    <p><h2>Filtering Physical Links</h2></p>
    <p>
        This simulation demonstrates between which nodes can a physical link be established.
        In addition we show how the <i>packetFilter</i> parameter works.
    </p>
    <p>
        We use the following network for this configuration:
    </p>
    <p>
        <img class="screen" src="PhysicalLinkVisualizerFiltering.png">
    </p>
    <p>
        This is a simple, infrastructure mode wireless topology, it consists of one <var>accessPoint</var> and three <var>wirelessHosts</var>.
        In addition there is an <var>IntegratedVisualizer</var>, an <var>IPv4NetworkConfigurator</var> and an <var>Ieee80211ScalarRadioMedium</var> module.<br>
        The <var>source</var> node pings the <var>destination</var> node.
        The <var>host1</var> doesn't generate any traffic except for connect to the access point.
        The antennas performance is limited to ensure that the <var>source</var> doesn't reach the <var>destination</var> directly.
        The visualization is set to only display the ping packets.
        We adjust the fading parameters, so the links don't fade out completely before the next ping packets come.
        Here is the configuration of the visualization:
        <div class="snippet">
            *.visualizer.*.physicalLinkVisualizer.displayLinks = true<br>
            *.visualizer.*.physicalLinkVisualizer.packetFilter = "ping*"<br>
            *.visualizer.*.physicalLinkVisualizer.fadeOutMode = "simulationTime"<br>
            *.visualizer.*.physicalLinkVisualizer.fadeOutTime = 1.3s
        </div>
    </p>
    <p>
        This video illustrates what happens when the simulation is run.
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=688 height=343 src="DisplayPingPhysicalLinks.mp4">
    </p>
    <p>
        
        <br>
        When the <var>accessPoint</var> receives the ping packet from the <var>source</var> on the physical layer, the visualizer displays a dotted arrow between them.
        Altough <var>host1</var> is not the destination of the ping message, an arrow still appears between the <var>source</var> and <var>host1</var>, because <var>host1</var> receives the ping packet correctly on the first layer.
        There is no physical link between the source and the destination.
        The radio signals are too weak, so destination can't receive the ping message flawlessly on the physical layer.<br>
        The <var>accessPoint</var> is able to create a physical link with any endpoint.
        When it sends a ping message an arrow is shown towards every endpoint.
        The <var>destination</var> sends <i>ping reply</i> messages.
        It only reaches the <var>accessPoint</var>, so there is only one link whose starting point is the <var>destination</var>.
    </p>


	<p><h2>Displaying Dynamic Physical Links</h2></p>
    <p>
        The goal of this configuration is displaying dynamically changing active physical links in a wireless environment.
        The physical links show which nodes are able to communicate with each other directly.
        Here is the network for this configuration:
    </p>
    <p>
        <img class="screen" src="PhysicalLinkVisualizerDynamic.png">
    </p>
    <p>
        The network contains an <var>IntegratedVisualizer</var>, an <var>IPv4NetworkConfigurator</var> and an <var>Ieee80211ScalarRadioMedium</var> module.
        The topology consists of seven <var>AODVRouters</var>, they move randomly between predefined borders.
        The nodes have two UDP applications: an <var>UDPBasicApp</var> and an <var>UDPSink</var>.
        The broadcast messages are sent by the <var>UDPBasicApp</var> and they are received by the <var>UDPSink</var>.
        We set the packets' name to <i>Broadcast</i>.
        We display only these messages.
        Here is the configuration of the visualization:
        <div class="snippet">
            *.visualizer.*.physicalLinkVisualizer.displayLinks = true<br>
            *.visualizer.*.physicalLinkVisualizer.packetFilter = "*Broadcast*"<br>
            *.visualizer.*.physicalLinkVisualizer.fadeOutMode = "simulationTime"<br>
            *.visualizer.*.physicalLinkVisualizer.fadeOutTime = 1.3s<br>
        </div>
    </p>
    <p>
        Here is what happens, when we run the simulation:
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=512 height=669 src="PhysicalLinkDynamic.mp4">
    </p>
    <p>
        Due to the broadcast packets, dotted arrows appear between the nodes.
        The arrows point to that nodes which received a <i>Broadcast</i> packet in the physical layer.
        As a result of the movement of nodes, some physical links disappear and some new links are established between the nodes.
    </p>


    <p><h2>More Information</h2></p>

    <p>
        This example only demonstrated the key features of phyisical link visualization. For more information, refer to the 
<var>PhysicalLinkVisualizer</var> NED documentation.
    </p>

</body>
</html>
