<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../../samples.css">
	<title>Network Route Visualization Example</title>

</head>

<body>
	<h1><center>Network Route Visualization Example</center></h1>

	<p><h2>Goals</h2></p>
    <p>
        In complex or adhoc networks, there are a lot of simultaneous communication.
        It is difficult to track individual packet paths, because we have to pay attention to more than one message at the same time.
        The network topology changes, e.g a router goes offline, also have an effect on the packets' routes.
        The network route visualization gives a clear overview of the individual packet paths.
        It highlights the network routes. When the network path is changed, the visualization changes too.
    </p>
    <p>    
        The example consists of four simulation models, each demonstrating different features of network route visualization.
    </p>	

    <p><h2>About the Visualizer</h2></p>
    <p>
        The <var>NetworkRouteVisualizer</var> keeps track of active network routes.
        A network route between two nodes is considered active, if a packet reaches the destination's network layer.
        By default, routes are not displayed, we can enable their vsisualization by switching the <i>displayRoutes</i> parameter to true.
	</p>   
    <p> 
        The links are represented visually by solid arrows.
        The arrow's starting point is the source node and its endpoint is the destination node.
        The arrow goes through those intermediate devices which are the part of the packet's network path.
    </p>
    <p>    
        The visualizer has filtering parameters, like <i>nodeFilter</i>, <i>interfaceFilter</i> and <i>packetFilter</i>.
        Using these we can specify which network routes are shown. By default all nodes, all interfaces and all packets are considered for displaying network routes.
        It is possible to use logical expressions in the values of the filter parameters, e.g <i>packetFilter = "ping* or tcp*"</i> shows the ping and the tcp network routes.
    </p>       


	<p><h2>Enabling Visualization of Network Routes</h2></p>
    <p>
        In this simulation we show how to enable the network route visualization with its default settings.
    </p>
    <p>
        The network for this configuration contains two <var>Standardhosts</var>, an <var>IPv4NetworkConfigurator</var> module and a <var>NetworkRouteVisualizer</var> module.
    </p>
    <p>
        <img class="screen" src="NetworkRouteSimple.png">
    </p>
    <p>
        The configuration is really simple.
        The <var>source</var> pings the <var>destination</var> and an additional line enables the visualization with the <i>displayRoutes</i> parameter:
        <div class="snippet">
            *.routeVisualizer.*.displayRoutes = true<br>
        </div>
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=630 height=281 src="NetworkRouteSimple.mp4">
    </p>
    <p>
        The ping message activates a network route between the devices. The further packets, like ARP messages go up to the data link layer only, so they don't activate a network route. After 1 second in real time, the network route fades out completely.
    </p>


    <p><h2>Displaying Static Network Routes</h2></p>
    <p>
        The simulation demonstrates the visualization of static routes in a non-trivial network.
        We also show how the <i>packetFilter</i> parameter works.
        Here is the network for this configuration:
    </p>
    <p>
        <img class="screen" src="NetworkRouteExtended.png">
    </p>
    <p>
        The topology consists of five routers, four switches and four <var>StandardHosts</var>. 
        There are two <var>source</var> hosts and two <var>destination</var> hosts.
        The <var>source1</var> pings <var>destination1</var>, and
        <var>source2</var> pings <var>destination2</var>.
        We enable the visualization of network routes with the <i>displayRoutes</i> parameter, but only when the message type is ping.
        We adjust the <i>fadeOutMode</i> and the <i>fadeOutTime</i> parameters, so the data link doesn't fade out completely before the next ping message arrives.
        <div class="snippet">
            *.visualizer.*.networkRouteVisualizer.displayRoutes = true<br>
            *.visualizer.*.networkRouteVisualizer.packetFilter = "ping*"<br>
            *.visualizer.*.networkRouteVisualizer.fadeOutMode = "simulationTime"<br>
            *.visualizer.*.networkRouteVisualizer.fadeOutTime = 1.4s
        </div>
    </p>
    <p>
        The following video shows what happens, when we start the simulation:
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=820 height=561 src="StaticNetworkRoute.mp4">
    </p>
    <p>
        Only the network routes of the ping messages are displayed because of the <i>packetFilter</i> parameter.
        Each arrow represents a different route.
        The arrows fade out gradually in simulation time, but not completely, because the <i>fadeOutTime</i> is greater than the sending interval of ping messages.
    </p>

    
    <p><h2>Displaying Dynamic Network Routes</h2></p>
    <p>
        In this simulation we show how the network routes are established and how they change between moving nodes.
        Here is the network that we use for this configuration:
    </p>
    <p>
        <img class="screen" src="DynamicNetworkRoutes.png">
    </p>
    <p>
        The network contains a <var>source</var>, a <var>destination</var> and five more <var>AODVRouters</var>.
        All nodes move randomly between predefined borders.
        The <var>source</var> pings the <var>destination</var>. In addition, there is an <var>IPv4NetworkConfigurator</var> module, an <var>IntegratedVisualizer</var> module and an <var>Ieee80211ScalarRadioMedium</var> module.
    </p>
    <p>
        The following video depicts what happens when the simulation is run:
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=610 height=665 src="DynamicNetworkRoute.mp4">
    </p>
    <p>
        When the ping reaches the destination's network layer, the visualizer shows the packet's network route.
        The nodes move randomly and their communication range is small, so the route will change between the source and the destination sooner or later.
        The visualizer displays the network route after the packet arrives to the destination, so we get information about the route changes.
    </p>


    <p><h2>Displaying Changing Network Routes</h2></p>
    <p>
        This configuration demonstartes how the visualizer reacts to the routing changes in a complex network.        
        Here is the network for this configuration:
    </p>  
    <p>
        <img class="screen" src="NetworkRouteComplex.png">
    </p>
    <p>
        The network contains four routers.
        They are placed and connected so as to create redundant paths.
        There is a wired and a wireless source-destination pair.
        Similarly to the previous configuration, the <var>wiredSource</var> pings the <var>wiredDestination</var> and the <var>wirelessSource</var> pings the <var>wirelessDestination</var>.
    </p>
    <p>
        We assign IP addresses manually by creating an xml configuaration file.
        We create a lifecycle for this configuration to turn off and on the routers at a given time.
        RIP is used for this configuration so the network routes change dynamically between the sources and the destinations, when a router goes offline.
        In this configuration we display only the ping echo messages.
        Here is the configuration for the visualization:
        <div class="snippet">
            *.visualizer.*.networkRouteVisualizer.displayRoutes = true<br>
            *.visualizer.*.networkRouteVisualizer.packetFilter = "ping* and not *reply"<br>
            *.visualizer.*.networkRouteVisualizer.fadeOutMode = "simulationTime"<br>
            *.visualizer.*.networkRouteVisualizer.fadeOutTime = 1.4s
        </div>        
    </p>  
    <p>
        Here is what happens, when we start the simulation:
    </p>
    <p align="center">        
        <video autoplay loop controls onclick="this.paused ? this.play() : this.pause();" width=704 height=591 src="ChangingNetworkRoutes.mp4">
    </p>
    <p>
        <ul>
            <li>At first, all ping packets go through <var>router1</var>.</li>
            <li>After a little time <var>router1</var> turns off and the network routes between sources and destinations are changed.
                The new routes go towards <var>router3</var>. During this time <var>router1</var> turns on again.</li>
            <li>After a few seconds the <var>router3</var> turns off and the routes are changed.
                The new routes go towards <var>router1</var> again.</li>
        </ul>
    </p>


    <p><h2>More Information</h2></p>
    <p>
        This example only demonstrated the key features of network route visualization. For more information, refer to the 
<var>NetworkRouteVisualizer</var> NED documentation.
    </p>
</body>
</html>
